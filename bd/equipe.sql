CREATE TABLE IF NOT EXISTS `equipe` (
  `ID` varchar(13) NOT NULL,
  `nom` varchar(255)  NOT NULL,
  `ville_origine` varchar(255),
  `description` varchar(255),
  `initiales` varchar(3) NOT NULL,
  `nom_logo` varchar(255),
  `type_logo` varchar(255),
  `path_logo` varchar(255),
-- Contraintes
    PRIMARY KEY (`ID`)
)DEFAULT CHARSET=utf8;