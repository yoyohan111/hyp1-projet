SET time_zone = 'SYSTEM';

CREATE TABLE IF NOT EXISTS `tournoi` (
  `ID` varchar(13) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_debut` DATE NOT NULL,
  `date_fin` DATE NOT NULL,
  `categorie` varchar(255),
  `anonce` varchar(1024),
  `nom_image` varchar(255),
  `type_image` varchar(255),
  `path_image` varchar(255),
-- Contraintes
    PRIMARY KEY (`ID`)
)DEFAULT CHARSET=utf8;