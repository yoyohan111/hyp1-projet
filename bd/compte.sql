CREATE TABLE IF NOT EXISTS compte(
    `ID` varchar(13) NOT NULL,
    `role` tinyint(1) NOT NULL DEFAULT 1,   -- 0 = Administrateur, 1 = Utilisateur
    `courriel` varchar(255) NOT NULL UNIQUE,
    `mdp` varchar(255) NOT NULL, -- Mot De Passe
    `prenom` varchar(255) NOT NULL,
    `nom` varchar(255),

-- Contraintes
    PRIMARY KEY (ID)
)DEFAULT CHARSET=utf8;

-- Création d'un Administrateur par défaut avec courriel: admin@admin.com et mot de passe: admin123
INSERT INTO `compte` (`ID`, `role`, `courriel`, `mdp`, `prenom`, `nom`) VALUES
('5ec9d4167a42d', 0, 'admin@admin.com', '$2y$10$WkxpLST5eUaSoo5lrhvK/.b.sFsSYHWe3IG/R9qGMcNKN4xoJQqH6', 'admin', '');