CREATE TABLE IF NOT EXISTS `relation_tournoi_equipe` (
    `tournoi_id` varchar(13) NOT NULL,
    `equipe_id` varchar(13) NOT NULL,
    `classement_equipe` tinyint(3),
-- Contraintes
    PRIMARY KEY (tournoi_id, equipe_id),
    FOREIGN KEY (tournoi_id) REFERENCES tournoi(ID) ON DELETE CASCADE,
    FOREIGN KEY (equipe_id) REFERENCES equipe(ID) ON DELETE CASCADE
)DEFAULT CHARSET=utf8;