CREATE TABLE IF NOT EXISTS `match` (
  `ID` varchar(13) NOT NULL,
  `id_tournoi` varchar(13) NOT NULL,
  `id_equipe1` varchar(13) NOT NULL,
  `id_equipe2` varchar(13) NOT NULL,
  `date` DATE NOT NULL,
  `heure` varchar(64),
  `lieu` varchar(255) NOT NULL,
  `epreuve` varchar(255) NOT NULL,
  `pointage_equipe1` tinyint(2) NOT NULL DEFAULT 0,
  `pointage_equipe2` tinyint(2) NOT NULL DEFAULT 0,
-- Contraintes
    PRIMARY KEY (`ID`),
    FOREIGN KEY (id_tournoi) REFERENCES tournoi(ID) ON DELETE CASCADE,
    FOREIGN KEY (id_equipe1) REFERENCES equipe(ID) ON DELETE CASCADE,
    FOREIGN KEY (id_equipe2) REFERENCES equipe(ID) ON DELETE CASCADE
)DEFAULT CHARSET=utf8;