 //_________________________________________________________________________________________________________________________________________________________________________________________________________________
//pour le navbar
$("#menu-toggle").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});


//_________________________________________________________________________________________________________________________________________________________________________________________________________________
//Pour le bouton qui reviens en haut de la page (jaune en bas à droite)
var btn = $("#btop");

$(window).scroll(function () {
  if ($(window).scrollTop() > 300) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

btn.on("click", function (e) {
  e.preventDefault();
  $("html, body").animate({ scrollTop: 0 }, "300");
});


//_________________________________________________________________________________________________________________________________________________________________________________________________________________
//Pour la page tableau_matchs

function SaisirResultatMatch(id_match,id_tournoi) {
  document.getElementById('btn_saisir_match_id=' + id_match).style.visibility = "hidden";
  document.getElementById('btn_supp_match_id=' + id_match).style.visibility = "hidden";
  document.getElementById('form_saisir_resultat_match_id=' + id_match).style.visibility = "visible";
}

function AnnulerSaisieResultatMatch(id_match) {
  document.getElementById('form_saisir_resultat_match_id=' + id_match).style.visibility = "hidden";
  document.getElementById('btn_saisir_match_id=' + id_match).style.visibility = "visible";
  document.getElementById('btn_supp_match_id=' + id_match).style.visibility = "visible";
} 

function SuppMatch(id_match,id_tournoi) {
  if(confirm("Voulez-vous vraiment supprimer ce match?")){
    window.location.href='?action=actionsAdmin&actionAdmin=suppMatch&match_id=' + id_match +'&tournoi_id=' + id_tournoi +'';
    return true;
  }
}

function editClassement(id_equipe,id_tournoi,nom_equipe,encien_classement) {
  if (encien_classement != false) {
    var classement = prompt("Changer le classement pour l'équipe "+ nom_equipe,encien_classement);
  } else {
    var classement = prompt("Entrer le nombre de points (classement) pour l'équipe "+ nom_equipe,0);
  }
  if (classement != null) {
    window.location.href='?action=actionsAdmin&actionAdmin=EditerClassementEquipe&equipe_id=' + id_equipe +'&tournoi_id=' + id_tournoi +'&classement_entree=' + classement +'';
    return true;
  }


}
  
function suppEquipe(id_equipe,id_tournoi) {
  if(confirm("Voulez-vous vraiment supprimer ce match?")){
    window.location.href='?action=actionsAdmin&actionAdmin=suppEquipeTournoi&equipe_id=' + id_equipe +'&tournoi_id=' + id_tournoi +'';
    return true;
  }

    var r = confirm("Voulez-vous vraiment Supprimer cette équie du tournoi?");
    if (r == true) {
      document.getElementById("{id_equipe pris par php}").remove();
    }
  }
  
  //tableau des calendriers des matchs avec recherche et tri!
  $(".mydatatable").DataTable({
    lengthMenu: [
      [5, 25, -1],
      [5, 25, "All"],
    ],
    scrollY: 400,
    scrollX: true,
    scrollCollapse: true,
    language: {
      lengthMenu: "Afficher _MENU_ données par page",
      zeroRecords: "Aucun matchs pour ce tournoi",
      info: "Page _PAGE_ sur _PAGES_",
      infoEmpty: "Aucunes données disponibles",
      infoFiltered: "(filtré à partir de _MAX_ enregistrements totaux)",
      search: "Recherche:",
      thousands: " ",
      processing: "En traitement...",
      decimal: ",",
      paginate: {
        first: "Premier",
        last: "Dernier",
        next: "Prochain",
        previous: "Précédent",
      },
    },
  });
  
//_________________________________________________________________________________________________________________________________________________________________________________________________________________
//Pour la page ajout_match pour l'ajout et suppression des matchs dans le DOM
//création des options du menu déroulant avec la liste des id et des noms des équipes passés avec PHP dans "ajout_match.php" (ligne 92)
var options_equipes = "";
if (typeof equipes_id !== 'undefined') {  //si la liste des équipes a été déclaré
  for (i = 0; i < equipes_id.length; i++) {
    options_equipes += "<option value='" + equipes_id[i] + "'>" + equipes_nom[i] + "</option>\r";
  }
}

var countNbMatch = 1;
function addMatch() {
countNbMatch += 1;

var lastT = document.getElementById("lesMatchs");

//ajout manuellement des composant du DOM pour l'ajout d'un nouveau match:
lastT.insertAdjacentHTML('beforeend',
'<div class="card mb-4" id="divmatch' + countNbMatch + '">' +
    '<div class="card-header d-flex justify-content-between">' +
        '<h3 class="m-0" id="h3match' + countNbMatch + '">+ Match ' + countNbMatch + ':</h3>' +
        '<button id="sup-match' + countNbMatch + '" type="button" class="btn btn-danger text-capitalize" onclick="removeMatch(' + countNbMatch + ')">supprimer</button>' +
    '</div>' +
    '<div class="card-body">' +
        '<div class="row">' +
            '<div class="form-group col-sm-12 col-md-5 col-lg-5">' +
                '<small class="form-text text-muted">Équipe 1</small>' +
                '<select class="form-control mt-2 mb-1" id="FormControlEquipe1Match' + countNbMatch + '" name="FormControlEquipe1Match' + countNbMatch + '" required>' +
                    '<option value="" disabled selected>-- choisir parmis les équipes du tournoi --</option>' +
                    options_equipes +
                '</select>' +
            '</div>' +
            '<p class="col-sm-12 col-md-2 col-lg-2 mb-3 pr-0 pl-0 text-center my-auto font-weight-bold text-uppercase">contre</p>' +
            '<div class="form-group col-sm-12 col-md-5 col-lg-5">' +
                '<small class="form-text text-muted">Équipe 2</small>' +
                '<select class="form-control mt-2" id="FormControlEquipe2Match'+ countNbMatch + '" name="FormControlEquipe2Match' + countNbMatch + '" required>' +
                    '<option value="" disabled selected>-- choisir parmis les équipes du tournoi--</option>' +
                    options_equipes +
                '</select>' +
            '</div>' +
        '</div>' +
        '<hr class="m-0"/>' +
        '<div class="form-group row">' +
            '<div class="col-lg-7 mt-2">' +
                '<label for="lieuMatch' + countNbMatch + '">Lieu:</label>' +
                '<input class="form-control" type="text" id="lieuMatch' + countNbMatch + '" name="lieuMatch' + countNbMatch + '" placeholder="Indiquer la ville et le nom du stade" required>' +
            '</div>' +
            '<div class="col-lg-5 mt-2">' +
                '<label>Épreuve éliminatoire:</label>' +
                '<select class="form-control" id="FormControlEpreuveMatch' + countNbMatch + '" name="FormControlEpreuveMatch' + countNbMatch + '" required>' +
                    '<option value="" disabled selected>-- choisir l\'épreuve de la compétition --</option>' +
                    '<option value="Finale">Finale</option>' +
                    '<option value="Demi-finales">Demi-finales</option>' +
                    '<option value="Quarts de finale">Quarts de finale</option>' +
                    '<option value="Huitièmes de finale">Huitièmes de finale</option>' +
                    '<option value="Seizièmes de finale">Seizièmes de finale</option>' +
                '</select>' +
            '</div>' +
        '</div>' +
        '<hr class="mt-0"/>' +
        '<div class="form-group row">' +
            '<div class="col-6 form-inline justify-content-center">' +
                '<label for="dateMatch' + countNbMatch + '" class="col-md-3 col-lg-2 pr-0 col-form-label">Date:</label>' +
                '<input class="form-control" type="date" id="dateMatch1" name="dateMatch' + countNbMatch + '" required>' +
            '</div>' +
            '<div class="col-6 form-inline justify-content-center">' +
                '<label for="heureMatch' + countNbMatch + '" class="col-md-4 col-lg-3 pr-0 col-form-label">Heure:</label>' +
                    '<input class="form-control" type="time" id="heureMatch' + countNbMatch + '" name="heureMatch' + countNbMatch + '">' +
            '</div>' +
        '</div>' +
    '</div>' +
'</div>'
);

$('input[name=nb_matchs]').val(countNbMatch);	//pour que l'application sache combien il y a de matchs..
}
function removeMatch(x) {   //pas mal de la SAUCE épaisse ça... xd
$("#divmatch" + x).remove();
$("#espace" + x).remove();

for (i = x + 1; i <= countNbMatch; i++) {	//Décaler toutes les numero de matchs après celle supprimé par 1.
    var y = i - 1;	//pour la décrémentation
    //div
    $('#divmatch' + i).prop('id', 'divmatch' + y);
    //h3 id et contenue (changer le contenue avant de changer le id car ça prend le id présent pour changer le contenue.....)
    document.getElementById('h3match' + i).innerHTML = '+ Match ' + y + ':';
    $('#h3match' + i).prop('id', 'h3match' + y);
    //bouton supprimer (modifier la fonction en premier! puisque ça le prend par le id...)
    $('#sup-match' + i).attr('onclick', 'removeMatch(' + y + ')');
    $('#sup-match' + i).prop('id', 'sup-match' + y);
    //select équipe 1:
    $('input[name=FormControlEquipe1Match' + i + ']').prop('name', 'FormControlEquipe1Match' + y);
    $('#FormControlEquipe1Match' + i).prop('id', 'FormControlEquipe1Match' + y);
    //select équipe 2:
    $('input[name=FormControlEquipe2Match' + i + ']').prop('name', 'FormControlEquipe2Match' + y);
    $('#FormControlEquipe2Match' + i).prop('id', 'FormControlEquipe2Match' + y);
    //lieuMatch:
    $('#lieuMatch' + i).attr('for', 'lieuMatch' + y);
    $('input[name=lieuMatch' + i + ']').prop('name', 'lieuMatch' + y);
    $('#lieuMatch' + i).prop('id', 'lieuMatch' + y);
    //Épreuve éliminatoire
    $('input[name=FormControlEpreuveMatch' + i + ']').prop('name', 'FormControlEpreuveMatch' + y);
    $('#FormControlEpreuveMatch' + i).prop('id', 'FormControlEpreuveMatch' + y);
    //dateMatch:
    $('#dateMatch' + i).attr('for', 'dateMatch' + y);
    $('input[name=dateMatch' + i + ']').prop('name', 'dateMatch' + y);
    $('#dateMatch' + i).prop('id', 'dateMatch' + y);
    //heureMatch:
    $('#heureMatch' + i).attr('for', 'heureMatch' + y);
    $('input[name=heureMatch' + i + ']').prop('name', 'heureMatch' + y);
    $('#heureMatch' + i).prop('id', 'heureMatch' + y);
    //nb Itération
    $('#InclureIteration' + i).prop('value', y);
    $('#InclureIteration' + i).prop('id', 'InclureIteration' + y);
    $('#LabelItt' + i).attr('for', 'InclureIteration' + y);
    $('#LabelItt' + i).prop('id', 'LabelItt' + y);
    //espace..
    $('#espace' + i).prop('id', 'espace' + y);
}
countNbMatch--;
$('input[name=nb_matchs]').val(countNbMatch);	//pour que l'application sache combien il y a de matchs..
}

//_________________________________________________________________________________________________________________________________________________________________________________________________________________
// script pour afficher le nom du fichier après le téléchargement (upload)
$(document).on('input', '.custom-file-input', function(e){ 
    $(this).next('.custom-file-label').text(e.target.files[0].name);
});

//_________________________________________________________________________________________________________________________________________________________________________________________________________________
//Script pour la page creation_tournoi
//le date picker avec le range
$('.input-daterange').datepicker({
    weekStart: 0,
    maxViewMode: 2,
    clearBtn: true,
    language: "fr",
    orientation: "top auto",
    multidate: false,
    todayHighlight: true
});

$('span#ErrorMessageEquipe').hide();

jQuery(function ($) { //pour s'assurer qu'au moins un checkbox est sélectionné pour la liste des élèves
    var requiredCheckboxes = $(':checkbox[required]');
    requiredCheckboxes.on('change', function (e) {
        var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr('name') + '"]');
        var isChecked = checkboxGroup.is(':checked');
        checkboxGroup.prop('required', !isChecked);
    });
    requiredCheckboxes.trigger('change');
});


//_________________________________________________________________________________________________________________________________________________________________________________________________________________
//Masques d'entrées
$('input[name="initialesEquipe"]').mask('SSS');  //masque pour les initiales de l'équipe
$('input[name="dateDebutTournoi"]').mask('00/00/00');
$('input[name="dateFinTournoi"]').mask('00/00/00');

//pour le masque de l'entrée du résultat d'un match
$('input[name="resultat_equipe1"]').mask('00');
$('input[name="resultat_equipe2"]').mask('00');
//pour le masque de l'entrée du classement d'une équipe
$('input[name="classement_input"]').mask('000');
