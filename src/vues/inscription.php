<div class="row justify-content-center">
    <div class="col-lg-7">
    <?php
        if (isset($_REQUEST["message_erreur"])) {
            echo "<div class='alert alert-danger'>";
            echo $_REQUEST["message_erreur"];
            echo "</div>";
        }
    ?>
        <div class="card shadow-lg border-0 rounded-lg mt-5 mb-5">
            <div class="card-header"><h3 class="text-center font-weight-light my-4">Créer un compte</h3></div>
            <div class="card-body">
                <form action="" method="POST">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Prénom</label>
                                <input class="form-control py-4" id="inputFirstName" name="prenom" type="text" placeholder="Entrer votre prénom" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Nom de famille</label>
                                <input class="form-control py-4" id="inputLastName" name="nom" type="text" placeholder="Entrez votre nom" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="small mb-1" for="inputEmailAddress">Adresse courriel</label>
                        <input class="form-control py-4" id="inputEmailAddress" name="courriel" type="email" aria-describedby="emailHelp" placeholder="Entrer votre adresse e-mail" required/
                        ></div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputPassword">Mot de passe</label>
                                <input class="form-control py-4" id="inputPassword" name="motPasse" type="password" placeholder="Entrez votre mot de passe" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"><label class="small mb-1" for="inputConfirmPassword">Confirmation du mot de passe</label>
                                <input class="form-control py-4" id="inputConfirmPassword" name="confMotPasse" type="password" placeholder="Confirmez le mot de passe" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0">
                        <input name="action" value="inscription" type="hidden" />
                        <input type="submit" class="btn btn-primary btn-block" value="Créer Compte"/>
                    </div>
                </form>
            </div>
            <div class="card-footer text-center">
                <div class="small"><a href="?action=vue&vue=connexion">Vous avez déjà un compte? Aller à la page de connexion</a></div>
            </div>
        </div>
    </div>
</div>