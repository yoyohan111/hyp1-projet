<?php 
    require_once('./modele/dao/TournoiDAO.class.php');
    require_once('./modele/classes/Tournoi.class.php');
?>
<h1 class="mb-5">Calendrier des tournois</h1>
<?php 
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }
?>


<div class="col p-0">

    <?php 
    $liste_tournois = TournoiDAO::findAll();
    
    if ($liste_tournois != false) {   //est false si vide
        foreach ($liste_tournois as $p) {
            if ($p != null) {   //afficher le tournoi:
                ?>
                <div class="card bg-primary text-white mb-4 ">
                    <div class='card-header'>
                        <div class='d-flex align-items-center <?php
                            if ($p->getCategorie() != null) {
                                echo "justify-content-between";
                            } 
                            ?>'>
                            <h4> <?= $p->getNom() ?> </h4>
                            <?php 
                                if ($p->getCategorie() != null) {
                                    echo '<p class="m-0">cathégorie:<strong class="pl-2">' . $p->getCategorie() . '</strong></p>';
                                }  ?>
                        </div>
                    </div>
                    <div class="card-body row pb-2">
                        <?php 
                            if ($p->getNomImage() != null) {
                                echo '<img class="col-sm-12 col-md-12 col-lg-5 mb-2" src="' . $p->getPathImage() . '" alt="image du tournoi">';
                            }
                            if ($p->getAnonce() != null) { //annonce du tournoi ici
                                echo '<p class="col-sm-12 col-md-12 col-lg-7">' . $p->getAnonce() . '</p>';
                            }
                        ?>
                        
                         
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="?action=vue&vue=tableau_matchs&tournoi_id=<?= $p->getId() ?>">Voir les matchs du tournoi</a>
                        <div class="small text-white"><span class='mr-3 font-weight-bold font-italic'>Du <?= $p->getDateDebut() ?> au <?= $p->getDateFin() ?> .</span><svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right"></i> --></div>
                    </div>
                </div>
            <?php 
            }
        }
    } else {
        ?>
        <div class="form-check">
            <p>Aucun Tournoi pour le moment. </p>
            <?php 
            if (isset($_SESSION["connecte"])) {
                if ($_SESSION["connecte"]["role"] == 0) {  //si un Administrateur est connecté:?>
                    <p>Veillez créer des tournois &nbsp;<button class='btn btn-warning' onclick='window.location.href = "?action=vue&vue=creation_tournoi";'><strong>ICI</strong></button></p>
                <?php }
            } ?>
            
        </div>
        <?php
    }
    ?>

</div>