<h1 class="mb-5">Créer un tournoi</h1>
<?php
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }
?>

<form action="" method="POST" enctype="multipart/form-data"> <!-- ici le enctype pour que l'inclusion du fichier fonctionne -->
    <div class="form-group row">
        <div class='col-sm-12 col-md-8 col-lg-9'>
            <label for="titreTournoi">Titre du tournoi</label>
            <input type="text" class="form-control" id="nomEquipe" name="nomTournoi" placeholder="Entrer le titre du tournoi" required>
        </div>
        <div class='col-sm-12 col-md-4 col-lg-3'>
            <label for="initialesEquipe">Catégorie d'âge:</label>
            <input type="text" class="form-control" id="categorie" name="categorieTournoi" placeholder="ex: majeurs (18-21 ans)">
        </div>
    </div>
    <!--        Sélection des équipes à inclure au tournoi -->
    <div class="form-group">
        <label class="noselect"><strong>Équipes à inclure au tournoi:</strong></label>
        <div class="accordion" id="accordionExample">
            <div class="card">
            <span id="ErrorMessageEquipe" class="warningMessage" style='color: red;'>Veuillez cocher au moins un Équipe</span>
                <div class="card-header" id="heading1">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed text-uppercase" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">liste des équipes <img name="req" src="styles/images/required_icon.svg" style="vertical-align: top;"/>
                        </button>
                    </h5>
                </div>
                
                <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="form-group">

                        <?php
                            include_once('./modele/dao/EquipeDAO.class.php');
                            require_once('./modele/classes/Equipe.class.php');
                            $liste_equipes = EquipeDAO::findAll();

                            if ($liste_equipes != false) {   //est false si vide
                                foreach ($liste_equipes as $p) {
                                    if ($p != null) {   //afficher l'équipe:
                                        ?>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" name="ListeIdEquipes[]" id="<?= $p->getId() ?>" value="<?= $p->getId() ?>" required="required">
                                            <label class="custom-control-label" for="<?= $p->getId() ?>">
                                                <?php 
                                                    echo $p->getNom();
                                                    if ($p->getNomLogo() != null) {
                                                        echo '&nbsp; <img width="25" src="' . $p->getPathLogo() . '" alt="logo de l`équipe">';
                                                    }
                                                ?>
                                            </label>
                                        </div>
                                    <?php 
                                    }
                                }
                            } else {
                                ?>
                                <div class="form-check">
                                    <p>Aucun équipe est inscrit à la Ligue! Veillez inscrire des equipes &nbsp;<button class='btn btn-warning' onclick="window.location.href = '?action=vue&vue=inscrire_equipe';"><strong>ICI</strong></button></p>
                                </div>
                                <?php
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="dateLabel" class="col-sm-12 col-md-4 col-lg-3 col-form-label">Dates du tournoi:</label>
        <div class="input-daterange input-group col-sm-12 col-md-8 col-lg-9" id="datepicker">
            <input type="text" class="input-sm form-control" name="dateDebutTournoi" placeholder="dates de début" title="format: {mm/dd/aaaa}" required/>
            <span class="input-group-addon ml-2 mr-2 my-auto"><strong>À</strong></span>
            <input type="text" class="input-sm form-control" name="dateFinTournoi" placeholder="date de fin" title="format: {mm/dd/aaaa}" required/>
        </div>
    </div>
    
    <hr/>
    <div class="form-group">
        <label for="imageTournoi">Image de présentation</label>
        <div class="custom-file file-input" style="display: flex">
            <input type="file" class="custom-file-input" name="imageTournoi" id="logoEquipe" accept="image/png, image/jpeg" style="cursor: pointer; text-indent: -999px;"/> <!-- ici le text-indent pour que le cursor: pointer fonctionne correctement -->
            <label class="custom-file-label" for="customFile" data-browse="Parcourir"><i class="fas fa-file-image fa-lg"></i><span class='text-secondary'>&nbsp Inclure un image en format jpeg ou png</span></label>
        </div>
    </div>
    <div class="form-group">
        <label for="desciptionTournoi">Anonce du tournoi</label>
        <textarea class="form-control" id='anonceTournoi' name="anonceTournoi" rows="3" placeholder="Inscrire une petite anonce pour captiver l'attention des utilisateurs"></textarea>
    </div>
    <hr>
    <input name="action" value="creerTournoi" type="hidden" />
    <input type="submit" class="btn btn-primary font-weight-bold text-capitalize mb-5" value="créer" />
</form>