<h1 class="mb-5">Inscrire une équipe à la ligue</h1>
<?php
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }
?>

<form action="" method="POST" enctype="multipart/form-data"> <!-- ici le enctype pour que l'inclusion du fichier fonctionne -->
    <div class="form-group row">
        <div class='col-sm-12 col-md-8 col-lg-9'>
            <label for="nomEquipe">Nom de l'équipe</label>
            <input type="text" class="form-control" id="nomEquipe" name="nomEquipe" placeholder="Entrer le nom complet de l'équipe" required>
        </div>
        <div class='col-sm-12 col-md-4 col-lg-3'>
            <label for="initialesEquipe">Initiales à 3 lettres</label>
            <input type="text" class="form-control" id="initialesEquipe" name="initialesEquipe" placeholder="CHM" required>
        </div>
    </div>
    <div class="form-group">
        <label for="villeEquipe">Ville d'origine</label>
        <input type="text" class="form-control" id="villeEquipe" name="villeEquipe" placeholder="Montréal" required>
    </div>
    <div class="form-group">
        <label for="logoEquipe">Logo</label>
        <div class="custom-file file-input" style="display: flex">
            <input type="file" class="custom-file-input" name="logoEquipe" id="logoEquipe" accept="image/png, image/jpeg" style="cursor: pointer; text-indent: -999px;"/> <!-- ici le text-indent pour que le cursor: pointer fonctionne correctement -->
            <label class="custom-file-label" for="customFile" data-browse="Parcourir"><i class="far fa-file-image fa-lg"></i><span class='text-secondary'>&nbsp; Inclure un image en format jpeg ou png</span></label>
        </div>
    </div>
    <div class="form-group">
        <label for="desciptionEquipe">Desciption de l'équipe</label>
        <textarea class="form-control" id='desciptionEquipe' name="desciptionEquipe" rows="3"></textarea>
    </div>
    <input name="action" value="inscrireEquipe" type="hidden" />
    <input type="submit" class="btn btn-primary font-weight-bold text-capitalize mb-5" value="inscrire" />
</form>