<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes" />
	<title>Ligue de Hockey de Rosemont</title>

	<!-- Bootstrap CDN -->
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" />
	<link href="styles/style.css" rel="stylesheet" />

	<!-- animation des icones de: https://l-lin.github.io/font-awesome-animation/-->
	<link rel="stylesheet" href="styles/font-awesome-animation.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
</head>

<body>
	<!-- ouvrir le body et le fermer dans le footer-->