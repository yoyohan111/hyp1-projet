<?php 
if (isset($_SESSION["connecte"])) { //prendre le type d'utilisateur connecté
    if ($_SESSION["connecte"]["role"] == 0) {
        $compte_role = 0;
    } else {
        $compte_role = 1;
    }
} else {
    $compte_role = -1;
}
?>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading border-bottom border-danger pr-0 pl-3">
            <div id="logo_container"><img class='img-fluid' src="styles\images\logo3.png" alt="Logo" \></div>
            <span><strong>L</strong>igue <strong>H</strong>ockey <strong>R</strong>osemont</span>
        </div>
        <div class="list-group list-group-flush border-bottom border-top border-secondary">
            <div class='list-group-item list-group-item-action pt-3 pb-1 pl-2'>
                <span class='text-uppercase font-weight-bold'>Options des visiteurs:</span>
            </div>
            <a href="?action=vue&vue=calendrier_tournois" class="list-group-item list-group-item-action bg-light">
                Calendrier des tournois<i class="far fa-calendar-alt ml-2"></i>
            </a>
            <a href="?action=vue&vue=info" class="list-group-item list-group-item-action bg-light">
                À propos<i class="fas fa-info-circle ml-2"></i>
            </a>

            <?php 
                if ($compte_role === 0) {
                ?>
                    <div class='list-group-item list-group-item-action pt-3 pb-1 pl-2'>
                        <span class='text-uppercase font-weight-bold'>Options d'Administrateur:</span>
                    </div>
                    <a href="?action=vue&vue=inscrire_equipe" class="list-group-item list-group-item-action bg-light">
                        Inscrire une équipe<i class="fas fa-plus ml-2"></i>
                    </a>
                    <a href="?action=vue&vue=creation_tournoi" class="list-group-item list-group-item-action bg-light">
                        Créer un tournoi<i class="far fa-calendar-plus ml-2"></i>
                    </a>
                <?php
                }
            ?>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->



    <!-- Page Content -->

    <?php
    //pour pouvoir changer la couleur du background avec la classe bg-primary de Bootstrap
    if (isset($_POST["content-class"])) {
        echo "<div id='page-content-wrapper' class='" . $_POST['content-class'] . "'>";
    } else {
        echo "<div id='page-content-wrapper' class='bg-light'>";
    }
    ?>

    <div id="page-content-wrapper" class='bg-light'>

        <nav class="navbar navbar-expand-sm navbar-dark bg-dark d-flex justify-content-between">

            <button class="btn btn-primary" id="menu-toggle"><span class="pr-2 faa-parent animated-hover"><i
                        class="fas fa-bars fa-lg faa-horizontal"></i></span>MENU</button>

            <a class="navbar-brand justify-content-center m-0">
                <img width="80" src="styles/images/logo3.png">
            </a>


            <div class='d-flex align-content-end' id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active dropdown show border border-white">
                        <a class="nav-link dropdown-toggle p-2" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="pr-2 pl-1 faa-parent animated-hover">
                                <i class="fas fa-user fa-lg faa-float"></i>
                            </span>
                            <span>
                                <?php 
                                    if (isset($_SESSION["connecte"]["prenom"])){
                                        echo $_SESSION["connecte"]["prenom"];
                                    } else {
                                        echo "Utilisateur";
                                    }
                                ?>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="?action=vue&vue=connexion">Connexion</a>
                            <a class="dropdown-item" href="?action=vue&vue=inscription">Inscription</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="?action=deconnexion">Déconnexion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <?php
        //pour pouvoir changer la couleur du background avec la classe bg-primary de Bootstrap
        if (isset($_POST["content-class"])) {
            echo "<main class='" . $_POST['content-class'] . "'>";
        } else {
            echo "<main class=''>";
        }
        ?>

        <main class='pt-3'>
            <div class="container">
                <!-- Bouton pour aller au début de la page -->
                <a id="btop"></a>

                <!-- PAGE CONTENT... -->