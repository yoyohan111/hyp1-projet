<?php 
if (!isset($_GET["tournoi_id"])) {
    $_REQUEST["message_erreur"] = "Vous devez sélectionner un tournoi de la liste des tournois.";
    //header("?action=vue&vue=calendrier_tournois");
}
require_once('./modele/dao/TournoiDAO.class.php');
require_once('./modele/classes/Tournoi.class.php');
require_once('./modele/dao/MatchDAO.class.php');
require_once('./modele/classes/Match.class.php');
require_once('./modele/dao/EquipeDAO.class.php');
require_once('./modele/classes/Equipe.class.php');

if (isset($_GET["tournoi_id"])) {
    try {
        $T = TournoiDAO::find($_GET["tournoi_id"]);
    } catch (\Throwable $th) {
        throw $th;
    }
    if ($T == null) {
        $_REQUEST["message_erreur"] = "Incappable de trouver le tournoi avec le id de {" . $_GET["tournoi_id"] ."}";
        ?>
            <script type="text/javascript">
                window.location.href = '?action=vue&vue=calendrier_tournois';
            </script>
        <?php 
    }
} else {
    $_REQUEST["message_erreur"] = "Le id du tournoi n'a pas été passé en paramêtre!";
    ?>
    <script type="text/javascript">
        window.location.href = '?action=vue&vue=calendrier_tournois';
    </script>
<?php 
}


?>


<h1 class="mb-5">Matchs du <?= $T->getNom() ?></h1>
<?php 
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }
?>

<div class="card mb-4">
    <div class="card-header d-flex<?php
        if ($compte_role === 0) {
            echo " justify-content-between";
        } 
        ?>">
        <h5 class='my-auto'><i class="fas fa-table mr-1"></i> Calendrier des Matchs à venir</h5>
        <?php
        if (isset($_SESSION["connecte"])) {
            if ($compte_role === 0) {
            ?>
            <button id="ajoutMatch" type="button" class="btn btn-success text-capitalize" onclick="location.href='?action=vue&vue=ajout_match&tournoi_id=<?= $T->getId() ?>'"><span class="fas fa-plus fa"></span> Ajouter un Match</button>
        <?php } }?>
        
    </div>
    <div class="card-body">
        <?php 
        $liste_matchs_a_venir = MatchDAO::getMatchsAVenir($T->getId());

        if ($liste_matchs_a_venir != false) {   //est false si vide ?>
            <table class="table table-striped mydatatable">
                <thead>
                    <tr>
                        <th>Équipes</th>
                        <th>Épreuve éliminatoire</th>
                        <th>Date <i class="fas fa-calendar-day"></i></th>
                        <th>Heure <i class="fas fa-clock"></i></th>
                        <th>Lieu <i class="fas fa-map-marker-alt"></i></th>
                        <!-- bouton d'actions que visible par un administrateur (role = 0) -->
                        <?php if ($compte_role === 0) {echo '<th>Actions <i class="fas fa-wrench"></i></th>'; } ?>
                    </tr>
                </thead>
                <tbody class="body-datatable">
                
                <?php 
                foreach ($liste_matchs_a_venir as $M) {
                    if ($M != null) {   //$M est un match
                        $E1 = EquipeDAO::find($M->getIdEquipe1());
                        $E2 = EquipeDAO::find($M->getIdEquipe2());

                        ?>
                        <tr class='my-auto' id="{id_match pris par php}">
                            <td class='text-nowrap my-auto'>
                                <div>
                                    <?php
                                    echo $E1->getNom();
                                    if ($E1->getNomLogo() != null) {
                                        echo '&nbsp; <img width="25" src="' . $E2->getPathLogo() . '" alt="logo de l`équipe">';
                                    }
                                    $classement_pts = EquipeDAO::get_classement($E1->getId(),$T->getId());
                                    if ($classement_pts != false) {
                                        echo "<small class='text-muted'>(" . $classement_pts . "pts)</small>";
                                    } ?>
                                </div>
                                <small class='font-weight-bold my-auto' m-0>contre</small>
                                <div>
                                    <?php
                                    echo $E2->getNom();
                                    if ($E2->getNomLogo() != null) {
                                        echo '&nbsp; <img width="25" src="' . $E2->getPathLogo() . '" alt="logo de l`équipe">';
                                    }
                                    $classement_pts = EquipeDAO::get_classement($E2->getId(),$T->getId());
                                    if ($classement_pts != false) {
                                        echo "<small class='text-muted'>(" . $classement_pts . "pts)</small>";
                                    } ?>
                                </div>
                            </td>
                            <td class='text-nowrap'><?= $M->getEpreuve() ?></td>
                            <td class='text-nowrap'><?= $M->getDate() ?></td>
                            <?php 
                                if ($M->getHeure() != null) {
                                    echo "<td class='text-nowrap'>" . $M->getHeure() . "</td>";
                                } else {
                                    echo "<td class='text-nowrap'>-</td>";
                                }
                            ?>
                            <td class='text-nowrap'><?= $M->getLieu() ?></td>
                            <?php if ($compte_role === 0) { 
                                ?>
                                <td class='text-nowrap'>
                                    <form style="visibility: hidden;" action="?action=actionsAdmin&actionAdmin=SaisirResultatMatch&match_id=<?= $M->getId() ?>&tournoi_id=<?= $T->getId() ?>" method="POST" class="form_saisir_resultat_match" id="form_saisir_resultat_match_id=<?= $M->getId() ?>">
                                        <input class="form-control inline" type="number" id="dateMatch1" name="resultat_equipe1" title="Équipe 1" placeholder="équipe 1" pattern="[0-9]{2}" required>
                                        <input class="form-control inline mb-2" type="number" id="dateMatch1" name="resultat_equipe2" title="Équipe 2" placeholder="équipe 2" pattern="[0-9]{2}" required>
                                        <button type='submit' class='btn btn-primary mr-2 mb-2 border-secondary' id="btn_ajouter_saisie_resultat_match_id=<?= $M->getId() ?>" >Ajouter Résultat</button>
                                        <button type='button' class='btn btn-secondary mr-2 mb-2 border-secondary' id="btn_annuler_saisie_resultat_match_id=<?= $M->getId() ?>" onclick="AnnulerSaisieResultatMatch('<?= $M->getId() ?>')">Annuler</button>
                                    </form>
                                    <button type='button' class='btn btn-primary mr-2 mb-2 border-secondary' id="btn_saisir_match_id=<?= $M->getId() ?>" onclick="SaisirResultatMatch('<?= $M->getId() ?>','<?= $T->getId() ?>')">Saisir le résultat</button>
                                    <button class='btn btn-danger mb-2 inline border-secondary' id='btn_supp_match_id=<?= $M->getId() ?>' onclick="SuppMatch('<?= $M->getId() ?>','<?= $T->getId() ?>')">Supprimer</button>

                                </td>
                            <?php } ?>
                        </tr>
                    <?php 
                    } 
                }
                ?>
                </tbody>
            </table>
        <?php
        } else {
            echo "<h4 style='color: red;'>Aucun Match à venir dans ce tournoi pour le moment</h4>";
            echo "veuillez vous connecter comme adminitrateur pour ajouter un match.";
        }?>

    </div>
</div>

<div class="card mb-4">
    <div class="card-header">
        <h5><i class="fas fa-hockey-puck mr-1"></i> Feuille de Résultat des matchs</h5>
    </div>
    <div class="card-body">
        <?php 
        $liste_matchs_finis = MatchDAO::getMatchsFinis($T->getId());

        if ($liste_matchs_finis != false) {   //est false si vide
            foreach ($liste_matchs_finis as $M) {
                if ($M != null) {   //$M est un match
                    $E1 = EquipeDAO::find($M->getIdEquipe1());
                    $E2 = EquipeDAO::find($M->getIdEquipe2());
                    ?>
                    <div class="card bg-light border-primary container mb-3">   <!-- un match -->
                        <span class='text-center mb-1' style="color: darkred;"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $M->getLieu(); ?></span>
                        <h5 class='text-center text-uppercase'> Le <?php echo $M->getDate();
                            if ($M->getHeure() != null) {
                                echo ", " . $M->getHeure();
                            }
                            echo "</h5>";
                            ?>
                        <hr class='mt-0 mb-2'>
                        <div class="row">
                            <div class="col-6 text-center"> <!-- pour l'équipe 1 -->
                                <?php 
                                if ($E1->getNomLogo() != null) {
                                    echo '<img class="mw-100 mh-100" src="' . $E1->getPathLogo() . '" alt="(logo_equipe)">';
                                } ?>
                                
                                <section class="info_equipe list-inline">
                                    <h4 class='text-uppercase font-weight-bold mb-0'><?= $E1->getNom() ?></h4>
                                    <?php 
                                        $pts = EquipeDAO::get_classement($E1->getId(), $T->getId());
                                        if ($pts != false) {
                                            echo '<span class="text-muted">(' . $pts . ' pts)</span>';
                                        }
                                    ?>
                                </section>
                            </div>
                            <div class="col-6 text-center"> <!-- pour l'équipe 2 -->
                                <?php 
                                if ($E2->getNomLogo() != null) {
                                    echo '<img class="mw-100 mh-100" src="' . $E2->getPathLogo() . '" alt="(logo_equipe)">';
                                } ?>
                                
                                <section class="info_equipe list-inline">
                                    <h4 class='text-uppercase font-weight-bold mb-0'><?= $E2->getNom() ?></h4>
                                    <?php 
                                        $pts = EquipeDAO::get_classement($E2->getId(), $T->getId());
                                        if ($pts != false) {
                                            echo '<span class="text-muted">(' . $pts . ' pts)</span>';
                                        }
                                    ?>
                                </section>
                            </div>
                        </div>
                        <div class='row mb-3 mt-3'>
                            <h1 class='col-6 col-sm-6 text-center font-weight-bold'><kbd class='pl-4 pr-4 pt-2 pb-2'><?= $M->getPointageEquipe1() ?></kbd></h1> <!-- ici le kbd pour le rendre noire -->
                            <h1 class='col-6 col-sm-6 text-center font-weight-bold'><kbd class='pl-4 pr-4 pt-2 pb-2'><?= $M->getPointageEquipe2() ?></kbd></h1>
                        </div>
                    </div>
                <?php }
            }
        } else {
            echo "<p>Le tournoi n'a pas encore été commencé. Aucun match complétés pour le moment.</p>";
        }
        ?>
    </div>
</div>


<div class="card mb-4">
    <div class="card-header">
        <h5><i class="fas fa-list"></i> Liste des équipes inscrits au tournoi avec leurs classement</h5>
    </div>
    <div class="card-body">
        <?php 
        $liste_equipes = EquipeDAO::findAllForTournoi($T->getId());

        if ($liste_equipes != false) {   //est false si vide
            foreach ($liste_equipes as $E) {
                if ($E != null) {   //$E est un équipe
                    ?>

                    <div class="card bg-light border-secondary container mb-2">   <!-- une équipe -->
                        <p class='p-2 m-0'>
                            <span class='align-middle text-nowrap'>
                                <?php 
                                echo "<strong>" . $E->getNom() . "</strong>";
                                if ($E->getNomLogo() != null) {
                                    echo '&nbsp; <img width="25" src="' . $E->getPathLogo() . '" alt="logo">';
                                } 
                                $pts = EquipeDAO::get_classement($E->getId(), $T->getId());
                                if ($pts != false) {
                                    echo '&nbsp;<small class="text-muted">(' . $pts . ' pts)</small>';
                                }
                            echo "</span>";
                            if ($compte_role === 0) { ?>
                                <button type="button" class="btn btn-danger float-right" id="btn_supp_equipe_id=<?= $E->getId() ?>" onclick="suppEquipe('<?= $E->getId() ?>','<?= $T->getId() ?>')">Supprimer du tournoi</button>
                                <button type="button" class="btn btn-primary float-right mr-2" onclick="editClassement('<?= $E->getId() ?>','<?= $T->getId() ?>','<?= $E->getNom() ?>','<?= $pts ?>')">Éditer son classement</button>
                            <?php } ?>
                        </p>
                    </div>
                <?php
                }
            }
        } else {
            echo "<p>Aucun équipe inscrit à ce tournoi!</p>";
            if ($compte_role === 0) {  //si un Administrateur est connecté: ?>
                <p>Veillez recréer ce tournoi en choisissant au moins 2 équipes! &nbsp;<button class='btn btn-warning' onclick='window.location.href = "?action=vue&vue=creation_tournoi";'><strong>ICI</strong></button></p>
            <?php }
        }
        ?>
    </div>
</div> 