<?php 
require_once('./modele/dao/TournoiDAO.class.php');
require_once('./modele/classes/Tournoi.class.php');
require_once('./modele/dao/MatchDAO.class.php');
require_once('./modele/classes/Match.class.php');
require_once('./modele/dao/EquipeDAO.class.php');
require_once('./modele/classes/Equipe.class.php');

if ($compte_role != 0) {
    ?>
        <script type="text/javascript">
            window.location.href = '?action=vue&vue=calendrier_tournois';
        </script>
    <?php 
}

if (isset($_GET["tournoi_id"])) {
    try {
        $T = TournoiDAO::find($_GET["tournoi_id"]);
    } catch (\Throwable $th) {
        throw $th;
    }
    if ($T == null) {
        $_REQUEST["message_erreur"] = "Incappable de trouver le tournoi avec le id de {" . $_GET["tournoi_id"] ."}";
        ?>
            <script type="text/javascript">
                window.location.href = '?action=vue&vue=tableau_matchs&tournoi_id=<?= $_GET["tournoi_id"] ?>';
            </script>
        <?php 
    }
}
?>
<h1 class="mb-5">Ajouter des matchs au tournoi "<?=$T->getNom() ?>"</h1>
<?php
if (isset($_REQUEST["message_erreur"])) {
    echo "<div class='alert alert-danger'>";
    echo $_REQUEST["message_erreur"];
    echo "</div>";
} elseif (isset($_REQUEST["message_succes"])) {
    echo "<div class='alert alert-success'>";
    echo $_REQUEST["message_succes"];
    echo "</div>";
}
?>


<form action="" method="POST">
<div id="lesMatchs">
    <div class='card mb-4' id="divmatch1">
        <div class='card-header d-flex justify-content-between'>
            <h3 class='m-0' id="h3match1">+ Match 1:</h3>
            <button id="sup-match1" type="button" class="btn btn-danger text-capitalize" onclick="removeMatch(1)">supprimer</button>
        </div>
        <div class='card-body'>
            <div class='row'>
                <?php 
                    try {
                        $liste_equipes = EquipeDAO::findAllForTournoi($T->getId());
                        $liste_id_equipes = array();
                        $liste_nom_equipes = array();
                    } catch (\Throwable $th) {
                        throw $th;
                    }
                    ?>

                <div class='form-group col-sm-12 col-md-5 col-lg-5'>
                    <small class="form-text text-muted">Équipe 1</small>
                    <select class="form-control mt-2 mb-1" id="FormControlEquipe1Match1" name="FormControlEquipe1Match1" required>
                        <option value="" disabled selected>-- choisir parmis les équipes du tournoi --</option>
                        <?php 
                        if ($liste_equipes != false) {  //si la liste n'est pas vide
                            foreach ($liste_equipes as $E) {
                                //à ne faire qu'une foix pour la liste passé à javascript
                                array_push($liste_id_equipes, $E->getId());
                                array_push($liste_nom_equipes, $E->getNom());
                                
                                echo '<option value="' . $E->getId() . '">' . $E->getNom() . '</option>';
                            }
                        }
                        ?>
                        <script>    //envoyer la liste des equipes à javascript pour qu'il puisse les populer quand l'utilisateur rajoute un match
                        var equipes_id = <?php echo json_encode($liste_id_equipes); ?>;
                        var equipes_nom = <?php echo json_encode($liste_nom_equipes); ?>;
                        //console.log(equipes_encoded);
                        </script>
                    </select>
                </div>
            
                <p class='col-sm-12 col-md-2 col-lg-2 mb-3 pr-0 pl-0 text-center my-auto font-weight-bold text-uppercase'>contre</p>

                <div class='form-group col-sm-12 col-md-5 col-lg-5'>
                    <small class="form-text text-muted">Équipe 2</small>
                    <select class="form-control mt-2" id="FormControlEquipe2Match1" name="FormControlEquipe2Match1" required>
                        <option value="" disabled selected>-- choisir parmis les équipes du tournoi --</option>
                        <?php 
                        try {
                            $liste_equipes = EquipeDAO::findAllForTournoi($T->getId());
                            if ($liste_equipes != false) {  //si la liste n'est pas vide
                                foreach ($liste_equipes as $E) {
                                    echo '<option value="' . $E->getId() . '">' . $E->getNom() . '</option>';
                                }
                            }
                        } catch (\Throwable $th) {
                            throw $th;
                        }
                        ?>
                    </select>
                </div>
            </div>
            <hr class='m-0'/> <!-- ligne d'espacement -->
            <div class="form-group row">
                <div class='col-lg-7 mt-2'>
                    <label for="lieuMatch1">Lieu:</label>
                    <input class="form-control" type="text" id="lieuMatch1" name="lieuMatch1" placeholder="Indiquer la ville et le nom du stade" required>
                </div>
                
                <div class='col-lg-5 mt-2'>
                    <label>Épreuve éliminatoire:</label>
                    <select class="form-control" id="FormControlEpreuveMatch1" name="FormControlEpreuveMatch1" required>
                        <option value="" disabled selected>-- choisir l'épreuve de la compétition --</option>
                        <option value="Finale">Finale</option>
                        <option value="Demi-finales">Demi-finales</option>
                        <option value="Quarts de finale">Quarts de finale</option>
                        <option value="Huitièmes de finale">Huitièmes de finale</option>
                        <option value="Seizièmes de finale">Seizièmes de finale</option>
                    </select>
                </div>
            </div>
            <hr class='mt-0'/> <!-- ligne d'espacement -->
            <div class="form-group row">
                <div class="col-6 form-inline justify-content-center"> <!-- créé un nouveau row ici -->
                    <label for="dateMatch1" class='inline'>Date: &nbsp;</label>
                    <input class="form-control inline" type="date" id="dateMatch1" name="dateMatch1" required>
                </div>
                <div class="col-6 form-inline justify-content-center">
                    <label for="heureMatch1">Heure: &nbsp;</label>
                    <input class="form-control" type="time" id="heureMatch1" name="heureMatch1">
                </div>
            </div>
        </div>

    </div>

</div> <!-- #lesMatches -->
<input name="nb_matchs" value="1" type="hidden" />	<!-- Pour savoir combien de Matchs il y a.. -->
<input name="tournoi_id" value="<?= $T->getId() ?>" type="hidden" />

<!-- Bouton pour l'ajout d'un nouveau match au tournoi -->
<div class="d-flex align-items-center justify-content-between mb-5">
    <button type="button" class="btn btn-info" onclick="addMatch()">
        <span class="fas fa-plus fa"></span>  Nouvau Match
    </button>

    <input name="action" value="ajoutMatch" type="hidden" />
    <input type="submit" class="btn btn-primary font-weight-bold text-capitalize" name="AjoutMatchsBouton" value="ajouter Matchs au tournoi" />
</div>
</form> <!-- fermeture de seul form de la page qui prend en charge tout les matchs -->