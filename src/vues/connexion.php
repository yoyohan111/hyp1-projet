<div class="row justify-content-center">
    <div class="col-lg-5">
    <?php
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }

    if (isset($_REQUEST["global_message"])) {
        $msg = "<span class=\"warningMessage\">" . $_REQUEST["global_message"] . "</span>";
    }
    ?>
        <div class="card shadow-lg border-0 rounded-lg mt-5">
            <div class="card-header"><h3 class="text-center font-weight-light my-4">Connexion</h3></div>
            <div class="card-body">
                <form action="" method="POST">
                    <div class="form-group">
                        <label class="small mb-1" for="inputEmailAddress">Adresse courriel</label>
                        <input class="form-control py-4" id="inputEmailAddress" name="courriel" type="email" placeholder="Entrer votre adresse e-mail" required/>
                        <?php
                            if (isset($_REQUEST["field_messages"]["courriel"])) {
                                echo "<span class=\"warningMessage\" style='color: red;'>" . $_REQUEST["field_messages"]["courriel"] . "</span>";
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="small mb-1" for="inputPassword">Mot de passe</label>
                        <input class="form-control py-4" id="inputPassword" name="motPasse" type="password" placeholder="Entrez votre mot de passe" required/>
                        <?php
                        if (isset($_REQUEST["field_messages"]["motPasse"])) {
                            echo "<span class=\"warningMessage\" style='color: red;'>" . $_REQUEST["field_messages"]["motPasse"] . "</span>";
                        }
                        ?>
                    </div>

                    <!-- TODO: --> 
                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                            <label class="custom-control-label" for="rememberPasswordCheck">Sauver l'authentification</label>
                        </div>
                        <input name="action" value="connexion" type="hidden" />
                        <input type="submit" class="btn btn-primary" value="Me connecter" />
                    </div>
                </form>
            </div>
            <div class="card-footer text-center">
                <div class="small"><a href="?action=vue&vue=inscription">Pas de compte? faites votre insciption ici!</a></div>
            </div>
        </div>
    </div>
</div>