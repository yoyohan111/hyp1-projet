<?php
require_once($_SERVER['DOCUMENT_ROOT']. '/hyp1-projet/src/modele/configs/config.php');
class Database
{
    private static $instance = null;
    
    public static function getInstance()    //avec PDO
    {
        if (self::$instance == null) {
            self::$instance = new PDO(
                "mysql:host=".Config::DB_HOST.";dbname=".Config::DB_NAME."",
                Config::DB_USER,
                Config::DB_PWD
            );
        }
        return self::$instance;
    }
}
