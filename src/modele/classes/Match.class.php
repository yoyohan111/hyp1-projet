<?php

class Match
{
    private $ID;
    private $id_tournoi;
    private $id_equipe1;
    private $id_equipe2;
    private $date;
    private $heure;
    private $lieu;
    private $epreuve;
    private $pointage_equipe1;
    private $pointage_equipe2;

    public function __construct()
    {
        $this->ID = uniqid();
        $this->pointage_equipe1 = 0; //0 par défaut
        $this->pointage_equipe2 = 0; //0 par défaut
    }

    //GETTERS
    public function getId()
    {
        return $this->ID;
    }

    public function getIdTournoi()
    {
        return $this->id_tournoi;
    }

    public function getIdEquipe1()
    {
        return $this->id_equipe1;
    }

    public function getIdEquipe2()
    {
        return $this->id_equipe2;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getHeure()
    {
        return $this->heure;
    }

    public function getEpreuve()
    {
        return $this->epreuve;
    }

    public function getLieu()
    {
        return $this->lieu;
    }

    public function getPointageEquipe1()
    {
        return $this->pointage_equipe1;
    }

    public function getPointageEquipe2()
    {
        return $this->pointage_equipe2;
    }

    //SETTERS
    public function setId($p)
    {
        $this->ID = $p;
    }

    public function setIdTournoi($p)
    {
        $this->id_tournoi = $p;
    }

    public function setIdEquipe1($p)
    {
        $this->id_equipe1 = $p;
    }

    public function setIdEquipe2($p)
    {
        $this->id_equipe2 = $p;
    }

    public function setDate($p)
    {
        $this->date = $p;
    }

    public function setHeure($p)
    {
        $this->heure = $p;
    }

    public function setLieu($p)
    {
        $this->lieu = $p;
    }

    public function setEpreuve($p)
    {
        $this->epreuve = $p;
    }

    public function setPointageEquipe1($p)
    {
        $this->pointage_equipe1 = $p;
    }

    public function setPointageEquipe2($p)
    {
        $this->pointage_equipe2 = $p;
    }

    public function loadFromRecord($ligne)
    {
        $this->ID = $ligne[0];
        $this->id_tournoi = $ligne[1];
        $this->id_equipe1 = $ligne[2];
        $this->id_equipe2 = $ligne[3];
        $this->date = $ligne[4];
        $this->heure = $ligne[5];
        $this->lieu = $ligne[6];
        $this->epreuve = $ligne[7];
        $this->pointage_equipe1 = $ligne[8];
        $this->pointage_equipe2 = $ligne[9];
    }
}
