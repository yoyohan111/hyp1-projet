<?php

class Equipe
{
    private $ID;
    private $nom;
    private $ville_origine;
    private $description;
    private $initiales;
    private $nom_logo;
    private $type_logo;
    private $path_logo;

    public function __construct()
    {
        $this->ID = uniqid();
    }

    //GETTERS
    public function getId()
    {
        return $this->ID;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getVille()
    {
        return $this->ville_origine;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getInitiales()
    {
        return $this->initiales;
    }

    public function getNomLogo()
    {
        return $this->nom_logo;
    }

    public function getTypeLogo()
    {
        return $this->type_logo;
    }

    public function getPathLogo()
    {
        return $this->path_logo;
    }

    //SETTERS
    public function setId($p)
    {
        $this->ID = $p;
    }

    public function setNom($p)
    {
        $this->nom = $p;
    }

    public function setVille($p)
    {
        $this->ville_origine = $p;
    }

    public function setDescription($p)
    {
        $this->description = $p;
    }

    public function setInitiales($p)
    {
        $this->initiales = $p;
    }

    public function setNomLogo($p)
    {
        $this->nom_logo = $p;
    }

    public function setTypeLogo($p)
    {
        $this->type_logo = $p;
    }

    public function setPathLogo($p)
    {
        $this->path_logo = $p;
    }

    public function loadFromRecord($ligne)
    {
        $this->ID = $ligne[0];
        $this->nom = $ligne[1];
        $this->ville_origine = $ligne[2];
        $this->description = $ligne[3];
        $this->initiales = $ligne[4];
        $this->nom_logo = $ligne[5];
        $this->type_logo = $ligne[6];
        $this->path_logo = $ligne[7];
    }
}
