<?php

class Compte
{
    private $ID = "";
    private $role = 1;
    private $courriel = "";
    private $mdp = "";
    private $prenom = "Default";
    private $nom = "";


    public function __construct()
    {
        $this->ID = uniqid();
    }

    public function getId()
    {
        return $this->ID;
    }
    
    public function getRole()
    {
        return $this->role;
    }

    public function getCourriel()
    {
        return $this->courriel;
    }

    public function getMdp()
    {
        return $this->mdp;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setId($p)
    {
        $this->ID = $p;
    }

    public function setRole($p)
    {
        $this->role = $p;
    }

    public function setCourriel($p)
    {
        $this->courriel = $p;
    }

    public function setMdp($p)
    {
        $this->mdp = $p;
    }

    public function setPrenom($p)
    {
        $this->prenom = $p;
    }

    public function setNom($p)
    {
        $this->nom = $p;
    }


    public function hasherMdp() //pour la sécurité
    {
        $this->mdp = password_hash($this->mdp, PASSWORD_DEFAULT);
    }

    public function verifierMdp($p_mdp)
    {
        return password_verify($p_mdp, $this->mdp);
    }


    public function loadFromRecord($ligne)
    {
        $this->ID = $ligne[0];
        $this->role = $ligne[1];
        $this->courriel = $ligne[2];
        $this->mdp = $ligne[3];
        $this->prenom = $ligne[4];
        $this->nom = $ligne[5];
    }

    public function __toString()
    {
        return "Compte[" . $this->ID . "," . $this->role . "," . $this->courriel . "," . $this->mdp . "," . $this->prenom . "," . $this->nom . "]";
    }

    public function affiche()
    {
        echo $this->__toString();
    }
}
