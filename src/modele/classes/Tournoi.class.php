<?php

class Tournoi
{
    private $ID;
    private $nom;
    private $date_debut;
    private $date_fin;
    private $categorie;
    private $anonce;
    private $nom_image;
    private $type_image;
    private $path_image;

    public function __construct()
    {
        $this->ID = uniqid();
    }

    //GETTERS
    public function getId()
    {
        return $this->ID;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getDateDebut()
    {
        return $this->date_debut;
    }

    public function getDateFin()
    {
        return $this->date_fin;
    }

    public function getCategorie()
    {
        return $this->categorie;
    }

    public function getAnonce()
    {
        return $this->anonce;
    }

    public function getNomImage()
    {
        return $this->nom_image;
    }

    public function getTypeImage()
    {
        return $this->type_image;
    }

    public function getPathImage()
    {
        return $this->path_image;
    }

    //SETTERS
    public function setId($p)
    {
        $this->ID = $p;
    }

    public function setNom($p)
    {
        $this->nom = $p;
    }

    public function setDateDebut($p)
    {
        $this->date_debut = $p;
    }

    public function setDateFin($p)
    {
        $this->date_fin = $p;
    }

    public function setCategorie($p)
    {
        $this->categorie = $p;
    }

    public function setAnonce($p)
    {
        $this->anonce = $p;
    }

    public function setNomImage($p)
    {
        $this->nom_image = $p;
    }

    public function setTypeImage($p)
    {
        $this->type_image = $p;
    }

    public function setPathImage($p)
    {
        $this->path_image = $p;
    }

    public function loadFromRecord($ligne)
    {
        $this->ID = $ligne[0];
        $this->nom = $ligne[1];
        $this->date_debut = $ligne[2];
        $this->date_fin = $ligne[3];
        $this->categorie = $ligne[4];
        $this->anonce = $ligne[5];
        $this->nom_image = $ligne[6];
        $this->type_image = $ligne[7];
        $this->path_image = $ligne[8];
    }
}
