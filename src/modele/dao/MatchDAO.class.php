<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Match.class.php');

class MatchDAO {

    public static function find($id) {
        $db = Database::getInstance();
        //{match} est une fonction dans SQL alors il faut s'assurer de mettre les apostrophes!
        $pstmt = $db->prepare("SELECT * FROM `match` WHERE ID = :x");
        $pstmt->execute(array(':x' => $id));       
        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Match();

        if ($result) {
            $p->setId($result->ID);
            $p->setIdTournoi($result->id_tournoi);
            $p->setIdEquipe1($result->id_equipe1);
            $p->setIdEquipe2($result->id_equipe2);
            $p->setDate($result->date);
            $p->setHeure($result->heure);
            $p->setLieu($result->lieu);
            $p->setEpreuve($result->epreuve);
            $p->setPointageEquipe1($result->pointage_equipe1);
            $p->setPointageEquipe2($result->pointage_equipe2);

            $pstmt->closeCursor();
            return $p;
        }
        
        $pstmt->closeCursor();
        
        return null;
    }

    public static function findAll() {
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM `match`");

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $match_object = array();
            
            foreach($liste_result as $m) {
                $match = new Match();
                $match->loadFromRecord($m);
                array_push($match_object, $match);
            }

            return($match_object);

        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function getMatchsAVenir($id_tournoi) {    //retourne la liste des matchs qui n'ont pas encore de score d'un tournoi spécifique
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM `match` WHERE id_tournoi = '" . $id_tournoi . "' AND pointage_equipe1 = 0 AND pointage_equipe2 = 0");

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $match_object = array();
            
            foreach($liste_result as $m) {
                $match = new Match();
                $match->loadFromRecord($m);
                array_push($match_object, $match);
            }

            return($match_object);

        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function getMatchsFinis($id_tournoi) {    //retourne la liste des matchs qui ont un score d'un tournoi spécifique
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM `match` WHERE id_tournoi = '" . $id_tournoi . "'AND pointage_equipe1 != 0 AND pointage_equipe2 != 0 ORDER BY date");

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $match_object = array();
            
            foreach($liste_result as $m) {
                $match = new Match();
                $match->loadFromRecord($m);
                array_push($match_object, $match);
            }

            return($match_object);

        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function create($x) {
        $request = "INSERT INTO `match` (ID ,id_tournoi ,id_equipe1, id_equipe2, date, heure, lieu, epreuve, pointage_equipe1, pointage_equipe2)" .
                " VALUES ('" . $x->getId() . "','" . $x->getIdTournoi() . "','" . $x->getIdEquipe1() . "','" . $x->getIdEquipe2() . "','" . $x->getDate() . "','" . $x->getHeure() . "','" . $x->getLieu() . "','" . $x->getEpreuve() . "','" . $x->getPointageEquipe1() ."','" . $x->getPointageEquipe2() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x) {
        $request = "UPDATE `match` SET ID = '" . $x->getId() . "', id_equipe1 = '" . $x->getIdEquipe1() . "', id_equipe2 = '" . $x->getIdEquipe2() . "', date = '" . $x->getDate() . "', heure = '" . $x->getHeure() ."', lieu = '" . $x->getLieu() ."', epreuve = '" . $x->getEpreuve() ."', pointage_equipe1 = '" . $x->getPointageEquipe1() . "', pointage_equipe2 = '" . $x->getPointageEquipe2() . "'" .
                " WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x) {
        $request = "DELETE FROM `match` WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
           throw $e;
        }
    }
}