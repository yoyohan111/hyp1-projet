<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Compte.class.php');
include_once('./modele/classes/ListeCompte.class.php');

class CompteDAO {

    public static function find($id) {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM compte WHERE ID = :x");
        $pstmt->execute(array(':x' => $id));       
        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Compte();

        if ($result) {
            $p->setId($result->ID);
            $p->setRole($result->role);
            $p->setCourriel($result->courriel);
            $p->setMdp($result->mdp);
            $p->setPrenom($result->prenom);
            $p->setNom($result->nom);

            $pstmt->closeCursor();
            return $p;
        }
        
        $pstmt->closeCursor();
        
        return null;
    }

    public static function findAll() {
        try {
            $liste = new ListeCompte();

            $requete = 'SELECT * FROM compte';
            $cnx = Database::getInstance();

            $res = $cnx->query($requete);
            foreach ($res as $row) {
                $c = new Compte();
                $c->loadFromRecord($row);
                $liste->add($c);
            }
            $res->closeCursor();
            $cnx = null;
            return $liste;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;  //retourne une liste d'objets Compte
        }
    }
    
    public static function findAllByRole($p_role) {
        try {
            $liste = new ListeCompte();
            $cnx = Database::getInstance();
            
            $pstmt = $cnx->prepare('SELECT * FROM compte WHERE role = :x ORDER BY prenom');
            $pstmt->execute(array(':x' => $p_role));
            
            $resultat = $pstmt->fetchAll(PDO::FETCH_NUM);
            if (count($resultat)== 0){
                return false;
            }
            
            foreach ($resultat as $row) {
                $c = new Compte();
                $c->loadFromRecord($row);
                $liste->add($c);
            }
            
            $pstmt->closeCursor();
            Database::FermerConnection();
            
            $cnx = null;         
            return $liste;  //retourne une liste d'objets Compte
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;  
        }
    }

    public function create($x) {
        $request = "INSERT INTO compte (ID ,role ,courriel, mdp, prenom, nom)" .
                " VALUES ('" . $x->getId() . "','" . $x->getRole() . "','" . $x->getCourriel() . "','" . $x->getMdp() . "','" . $x->getPrenom() . "','" . $x->getNom() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x) {
        $request = "UPDATE compte SET ROLE = '" . $x->getRole() . "', COURRIEL = '" . $x->getCourriel() . "', MDP = '" . $x->getMdp() . "', PRENOM = '" . $x->getPrenom() . "', NOM = '" . $x->getNom() . "'" .
                " WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x) {
        $request = "DELETE FROM compte WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
           throw $e;
        }
    }
    
    public static function getNbComptes() {
        try {
            $db = Database::getInstance();
            $request = $db->prepare("SELECT COUNT(*) FROM compte");
            $request->execute();
            return $request->fetchColumn();
        } catch (PDOException $e) {
           throw $e;
        }
    }
    
    public static function findByCourriel($courriel) {
        $db = Database::getInstance();
        $pstmt = $db->prepare("SELECT * FROM compte WHERE courriel = :x");
        $pstmt->execute(array(':x' => $courriel));

        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Compte();

        if ($result) {
            $p->setId($result->ID);
            $p->setRole($result->role);
            $p->setCourriel($result->courriel);
            $p->setMdp($result->mdp);
            $p->setPrenom($result->prenom);
            $p->setNom($result->nom);
            $pstmt->closeCursor();
            return $p;
        }
        $pstmt->closeCursor();
        return null;
    }
}