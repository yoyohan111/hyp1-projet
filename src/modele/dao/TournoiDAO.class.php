<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Tournoi.class.php');

class TournoiDAO {

    public static function find($id) {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM tournoi WHERE ID = :x");
        $pstmt->execute(array(':x' => $id));       
        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Tournoi();

        if ($result) {
            $p->setId($result->ID);
            $p->setNom($result->nom);
            $p->setDateDebut($result->date_debut);
            $p->setDateFin($result->date_fin);
            $p->setCategorie($result->categorie);
            $p->setAnonce($result->anonce);
            $p->setNomImage($result->nom_image);
            $p->setTypeImage($result->type_image);
            $p->setPathImage($result->path_image);

            $pstmt->closeCursor();
            return $p;
        }
        
        $pstmt->closeCursor();
        
        return null;
    }

    public static function findAll() {
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM tournoi");

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $tournoi_object = array();
            
            foreach($liste_result as $t) {
                $tournoi = new Tournoi();
                $tournoi->loadFromRecord($t);
                array_push($tournoi_object, $tournoi);
            }

            return($tournoi_object);

        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function create($x) {
        $request = "INSERT INTO tournoi (ID ,nom ,date_debut, date_fin, categorie, anonce, nom_image, type_image, path_image)" .
                " VALUES ('" . $x->getId() . "','" . $x->getNom() . "','" . $x->getDateDebut() . "','" . $x->getDateFin() . "','" . $x->getCategorie() . "','" . $x->getAnonce() . "','" . $x->getNomImage() . "','" . $x->getTypeImage() . "','" . $x->getPathImage() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x) {
        $request = "UPDATE tournoi SET nom = '" . $x->getNom() . "', date_debut = '" . $x->getDateDebut() . "', date_fin = '" . $x->getDateFin() . "', categorie = '" . $x->getCategorie() . "', anonce = '" . $x->getAnonce() . "', nom_image = '" . $x->getNomImage() ."', type_image = '" . $x->getTypeImage() ."', path_image = '" . $x->getPathImage() . "'" .
                " WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x) {
        $request = "DELETE FROM tournoi WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
           throw $e;
        }
    }
    
    public static function getNbTournois() {
        try {
            $db = Database::getInstance();
            $request = $db->prepare("SELECT COUNT(*) FROM tournoi");
            $request->execute();
            return $request->fetchColumn();
        } catch (PDOException $e) {
           throw $e;
        }
    }

    public static function faire_relation_tournoi_equipe($id_tournoi, $liste_id_equipes) { //$id_tournoi == array
        if ($liste_id_equipes == null) {
            return false;
        } elseif (gettype($liste_id_equipes) == "array") {    //$id_eleves est une liste contenant tout les ID des élèves assignés à la tâche
            for ($i = 0; $i < count($liste_id_equipes); $i++) {
                $request = "INSERT INTO relation_tournoi_equipe (tournoi_id, equipe_id)" .
                    " VALUES ('" . $id_tournoi . "','" . $liste_id_equipes[$i] . "')";
                try {
                    $db = Database::getInstance();
                    $db->exec($request);    //pas de return ici pour pouvoir rester dans la boucle
                } catch (PDOException $e) {
                    throw $e;
                    return false; //false pour l'échec de la requête
                }
            }
            return true;  //true pour la reéussite des requêtes
        } else {
            return false; //false si la requête n'a pas été effectué
        }
    }
}