<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Equipe.class.php');

class EquipeDAO
{
    public static function find($id)
    {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM equipe WHERE ID = :x");
        $pstmt->execute(array(':x' => $id));
        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Equipe();

        if ($result) {
            $p->setId($result->ID);
            $p->setNom($result->nom);
            $p->setVille($result->ville_origine);
            $p->setDescription($result->description);
            $p->setInitiales($result->initiales);
            $p->setNomLogo($result->nom_logo);
            $p->setTypeLogo($result->type_logo);
            $p->setPathLogo($result->path_logo);

            $pstmt->closeCursor();
            return $p;
        }
        
        $pstmt->closeCursor();
        
        return null;
    }

    public static function findAll()
    {
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM equipe ORDER BY nom");

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $equipes_object = array();
            
            foreach ($liste_result as $eq) {
                $equipe = new Equipe();
                $equipe->loadFromRecord($eq);
                array_push($equipes_object, $equipe);
            }

            return($equipes_object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function findAllForTournoi($id_tournoi)
    {
        try {
            $pdo = Database::getInstance();
            $stmt = $pdo->query("SELECT * FROM equipe WHERE ID IN (SELECT DISTINCT equipe_id FROM relation_tournoi_equipe WHERE tournoi_id = '" . $id_tournoi . "') ORDER BY nom");
            //ORDER BY nom pour prend par ordre des noms comme demandé dans l'énoncée du projet
            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();
                
                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $equipes_object = array();
            
            foreach ($liste_result as $eq) {
                $equipe = new Equipe();
                $equipe->loadFromRecord($eq);
                array_push($equipes_object, $equipe);
            }

            return($equipes_object);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function create($x)
    {
        $request = "INSERT INTO equipe (ID ,nom ,ville_origine, description, initiales, nom_logo, type_logo, path_logo)" .
                " VALUES ('" . $x->getId() . "','" . $x->getNom() . "','" . $x->getVille() . "','" . $x->getDescription() . "','" . $x->getInitiales() . "','" . $x->getNomLogo() . "','" . $x->getTypeLogo() . "','" . $x->getPathLogo() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x)
    {
        $request = "UPDATE equipe SET nom = '" . $x->getNom() . "', ville_origine = '" . $x->getVille() . "', description = '" . $x->getDescription() . "', initiales = '" . $x->getInitiales() . "', nom_image = '" . $x->getNomImage() ."', type_logo = '" . $x->getTypeLogo() ."', path_logo = '" . $x->getPathLogo() . "'" .
                " WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x)
    {
        $request = "DELETE FROM equipe WHERE ID = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    //POUR LE CLASSEMENT (getters et setters)

    public static function get_classement($id_equipe, $id_tournoi)
    {
        try {
            $db = Database::getInstance();

            $commandeSQL = $db->prepare("SELECT DISTINCT classement_equipe FROM relation_tournoi_equipe WHERE equipe_id = :e AND tournoi_id = :t");
            $commandeSQL->bindParam(':e', $id_equipe);
            $commandeSQL->bindParam(':t', $id_tournoi);

            //var_dump($commandeSQL);   //aucune idée pourquoi bindParam ne fonctionne pas!!!
            return $commandeSQL->execute();
        } catch (Exception $e) {
            throw new Exception("Impossible d’obtenir la connexion à la BD."); 
        }
    }

    public static function set_classement_equipe($id_equipe, $id_tournoi, $classement)    //$x est 
    {
        $request = "UPDATE relation_tournoi_equipe SET classement_equipe = '" . $classement . "'" .
                " WHERE tournoi_id = '" . $id_tournoi . "' AND equipe_id = '" . $id_equipe . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }
}
