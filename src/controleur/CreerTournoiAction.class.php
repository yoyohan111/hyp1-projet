<?php

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/TournoiDAO.class.php');
require_once('./modele/classes/Tournoi.class.php');

class CreerTournoiAction implements Action {
    public function execute() {

        if (!isset($_REQUEST["nomTournoi"]) || !isset($_REQUEST["ListeIdEquipes"]) || !isset($_REQUEST["dateDebutTournoi"]) || !isset($_REQUEST["dateFinTournoi"])) {
            $_REQUEST["message_erreur"] = "Des paramètres requis sont manquants dans le formulaire!";
            return "creation_tournoi";
        }
        if ($_REQUEST["nomTournoi"] == '' || $_REQUEST["ListeIdEquipes"] == '' || $_REQUEST["dateDebutTournoi"] == '' || $_REQUEST["dateFinTournoi"] == '') {
            $_REQUEST["message_erreur"] = "Des champs requis ont été laissés vides.";
            return "creation_tournoi";
        }

        //création du dao et objet du tournoi
        $Tdao = new TournoiDAO();
        $T = new Tournoi();

        try {
            $T->setNom($_REQUEST["nomTournoi"]);
            $ListeIdEquipes = $_POST["ListeIdEquipes"];

            //convertir la date en bon format pour la base de donnée (YYYY-MM-DD)
            $date_debut = date("Y-m-d", strtotime($_REQUEST["dateDebutTournoi"]));
            $date_fin = date("Y-m-d", strtotime($_REQUEST["dateFinTournoi"]));

            $T->setDateDebut($date_debut);
            $T->setDateFin($date_fin);

            //pour les autres entrées du formulaires qui ne sont pas "required"
            if (isset($_REQUEST["categorieTournoi"]) && $_REQUEST["categorieTournoi"] != '') {
                $T->setCategorie($_REQUEST["categorieTournoi"]);
            }
            if (isset($_REQUEST["anonceTournoi"]) && $_REQUEST["anonceTournoi"] != '') {
                $T->setAnonce($_REQUEST["anonceTournoi"]);
            }
            
            //pour le fichier
            if ($_FILES['imageTournoi']['name'] != "") { //S'il y a un image à inclure
                $file = $_FILES['imageTournoi'];

                $file_name = $file['name'];
                $file_type = $file['type'];
                $file_tmp = $file['tmp_name'];
                $file_size = $file['size'];
                $file_error = $file['error'];

                //prendre l'extention du fichier
                $file_ext = explode('.', $file_name);
                $file_ext = strtolower(end($file_ext));
                
                if  ($file_error === 0){
                    //ici uniqid puisque s'il y a déjà un fichier avec le même nom dans la base de donnée, ça ne va pas fonctionner..
                    //alors on crée un nom unique
                    $file_name_new = uniqid('', true) . '.' . $file_ext;
                    $file_path = 'uploads/image_tournois/' . $file_name_new;

                    if (move_uploaded_file($file_tmp, $file_path)) {    //si l'ajout du fichier a bien fonctionné, il le rajoute à l'objet de l'équipe
                        $T->setNomImage($file_name);
                        $T->setTypeImage($file_type);
                        $T->setPathImage($file_path);
                    } else {
                        $_REQUEST["message_erreur"] = "L'image n'a pas pu être enregistré!";
                        return "creation_tournoi";
                    }
                } else {
                    $_REQUEST["message_erreur"] = "Fichier non valide! Réessayer de nouveau.";
                    return "creation_tournoi";
                }
                unset($_FILES['imageTournoi']);   //unset pour le prochain fichier..
            }
        } catch (\Throwable $th) {
            $_REQUEST["message_erreur"] = "Erreur dans l'ajout de l'équipe à la ligue de hockey.";
            throw $th;
            return "creation_tournoi";
        }

        if (!$Tdao->create($T)) { //si il y a eu une erreure dans la requête sql pour l'ajout de l'équipe à la base de donnée:
            $_REQUEST["message_erreur"] = "Erreur dans l'ajout du Tournoi.";
            return "creation_tournoi";
        } else {  //si la requête sql a bien fonctionné:
            //faire la relation entre le tournoi et tout les euqipes:
            if (!TournoiDAO::faire_relation_tournoi_equipe($T->getId(), $ListeIdEquipes)) { //si la relation na pas été fait correctement:

            } else {
                //afficher le message de réussite
                $_REQUEST["message_succes"] = "Tournoi enregistré!";
                return "creation_tournoi";
            };
        }
        //return "creation_tournoi";
    }
}
?>