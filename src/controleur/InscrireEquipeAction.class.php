<?php

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/EquipeDAO.class.php');
include_once('./modele/classes/Equipe.class.php');

class InscrireEquipeAction implements Action {
    public function execute() {

        if (!isset($_REQUEST["nomEquipe"]) || !isset($_REQUEST["initialesEquipe"]) || !isset($_REQUEST["villeEquipe"])) {
            $_REQUEST["message_erreur"] = "Des paramètres requis sont manquants dans le formulaire!";
            return "inscrire_equipe";
        }
        if ($_REQUEST["nomEquipe"] == '' || $_REQUEST["initialesEquipe"] == '' || $_REQUEST["villeEquipe"] == '') {
            $_REQUEST["message_erreur"] = "Des champs requis ont été laissés vides.";
            return "inscrire_equipe";
        }

        //création du dao et objet de l'équipe
        $Edao = new EquipeDAO();
        $E = new Equipe();

        try {
            $E->setNom($_REQUEST["nomEquipe"]);
            $E->setInitiales($_REQUEST["initialesEquipe"]);
            $E->setVille($_REQUEST["villeEquipe"]);

            if (isset($_REQUEST["desciptionEquipe"]) && $_REQUEST["desciptionEquipe"] != '') {
                $E->setDescription($_REQUEST["desciptionEquipe"]);
            }
            
            //pour le fichier
            if ($_FILES['logoEquipe']['name'] != "") {
                $file = $_FILES['logoEquipe'];
                
                $file_name = $file['name'];
                $file_type = $file['type'];
                $file_tmp = $file['tmp_name'];
                $file_size = $file['size'];
                $file_error = $file['error'];

                //prendre l'extention du fichier
                $file_ext = explode('.', $file_name);
                $file_ext = strtolower(end($file_ext));
                
                if  ($file_error === 0){
                    $file_name_new = uniqid('', true) . '.' . $file_ext;
                    $file_path = 'uploads/logo_equipes/' . $file_name_new;

                    if (move_uploaded_file($file_tmp, $file_path)) {    //si l'ajout du fichier a bien fonctionné, il le rajoute à l'objet de l'équipe
                        $E->setNomLogo($file_name);
                        $E->setTypeLogo($file_type);
                        $E->setPathLogo($file_path);
                    } else {
                        $_REQUEST["message_erreur"] = "Le Logo n'a pas pu être enregistré!";
                        return "inscrire_equipe";
                    }
                } else {
                    $_REQUEST["message_erreur"] = "Fichier non valide! Réessayer de nouveau.";
                    return "inscrire_equipe";
                }
                unset($_FILES['logoEquipe']);   //unset pour le prochain fichier..
            }
        } catch (\Throwable $th) {
            $_REQUEST["message_erreur"] = "Erreur dans l'ajout de l'équipe à la ligue de hockey.";
            throw $th;
            return "inscrire_equipe";
        }

        if (!$Edao->create($E)) { //si il y a eu une erreure dans la requête sql pour l'ajout de l'équipe à la base de donnée:
            $_REQUEST["message_erreur"] = "Erreur dans l'ajout de l'équipe à la ligue de hockey.";
            return "inscrire_equipe";
        } else {  //si la requête sql a bien fonctionné:
            $_REQUEST["message_succes"] = "Équipe enregistré!";
            return "inscrire_equipe";
        }
    }
}