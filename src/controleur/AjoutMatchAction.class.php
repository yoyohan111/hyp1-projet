<?php
require_once('./controleur/Action.interface.php');
//importation des classes et DAOs utilisés
require_once('./modele/dao/TournoiDAO.class.php');
require_once('./modele/classes/Tournoi.class.php');
require_once('./modele/dao/MatchDAO.class.php');
require_once('./modele/classes/Match.class.php');
require_once('./modele/dao/EquipeDAO.class.php');
require_once('./modele/classes/Equipe.class.php');

class AjoutMatchAction implements Action {
    public function execute() {
        //regarder pour des erreurs avant de commencer
        if (isset($_REQUEST["tournoi_id"])) {
            try {
                $T = TournoiDAO::find($_REQUEST["tournoi_id"]);
            } catch (\Throwable $th) {
                throw $th;
            }
            if ($T == null) {
                $_REQUEST["message_erreur"] = "Incappable de trouver le tournoi avec le id de {" . $_GET["tournoi_id"] ."}";
                return "ajout-match";
            }
        } else {
            $_REQUEST["message_erreur"] = "Le id du tournoi n'a pas été passé en paramêtre!";
            return "ajout-match";
        }

        if (isset($_REQUEST['nb_matchs'])) {
            if ($_REQUEST['nb_matchs'] > 0) {
                $nb_matchs = $_REQUEST['nb_matchs'];
            } else {
                $_REQUEST["message_erreur"] = "Il n'y a aucun match!";
                return "ajout_match";
            }
        } else {
            $_REQUEST["message_erreur"] = "Erreur: nb_matchs n'est pas défini.";
            return "ajout_match";
        }

        for ($i = 1; $i <= $nb_matchs; $i++) {   //strictement plus petit puisque $i commence à 1
            if (!isset($_REQUEST['FormControlEquipe1Match' . $i]) ||!isset($_REQUEST['FormControlEquipe2Match' . $i]) ||!isset($_REQUEST['lieuMatch' . $i]) ||!isset($_REQUEST['FormControlEpreuveMatch' . $i]) ||!isset($_REQUEST['dateMatch' . $i])) {
                $_REQUEST["message_erreur"] = "Erreur: des paramêtres requis sont manquants!";
                return "ajout_match";
            } 
            if ($_REQUEST["FormControlEquipe1Match" . $i] == '' || $_REQUEST["FormControlEquipe2Match" . $i] == '' || $_REQUEST["lieuMatch" . $i] == '' || $_REQUEST["FormControlEpreuveMatch" . $i] == ''|| $_REQUEST["dateMatch" . $i] == '') {
                $_REQUEST["message_erreur"] = "Des champs requis ont été laissés vides.";
                return "ajout_match";
            }
            
            try {
                //Créer l'objet Match
                $M = new Match();
                $M->setIdTournoi($T->getId());
                $M->setIdEquipe1($_POST['FormControlEquipe1Match' . $i]);
                $M->setIdEquipe2($_POST['FormControlEquipe2Match' . $i]);
                $M->setDate($_POST['dateMatch' . $i]);
                $M->setLieu($_POST['lieuMatch' . $i]);
                $M->setEpreuve($_POST['FormControlEpreuveMatch' . $i]);

                if (isset($_REQUEST['heureMatch' + $i])) {
                    $M->setHeure($_POST['heureMatch' . $i]);
                }

                //enregistrer le match dans la base de donnée
                if (!MatchDAO::create($M)) { //si il y a eu une erreure dans la requête sql
                    $_REQUEST["message_erreur"] = "Erreur dans l'ajout d'un du match #" . $nb_matchs;
                    return "ajout_match";
                }
            } catch (\Throwable $th) {
                throw $th;
            }
        }
        //si tout a bien fonctionné faire le:
        if ($nb_matchs > 1) {
            $_REQUEST["message_succes"] = "Les " . $nb_matchs . " matchs ont bien étés enregistrés!";
        } else {
            $_REQUEST["message_succes"] = "Le match a bien étés enregistrés!";
        }
        return "ajout_match";
    }


        //return "ajout_match";
}