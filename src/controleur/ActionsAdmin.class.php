<?php
require_once('controleur/Action.interface.php');
//importation des classes et DAOs utilisés
require_once('./modele/dao/TournoiDAO.class.php');
require_once('./modele/classes/Tournoi.class.php');
require_once('./modele/dao/MatchDAO.class.php');
require_once('./modele/classes/Match.class.php');
require_once('./modele/dao/EquipeDAO.class.php');
require_once('./modele/classes/Equipe.class.php');

class ActionsAdmin implements Action {
    public function execute() {
        //regarder pour des erreurs avant de commencer
        if (isset($_REQUEST["tournoi_id"])) {   //si le id du tournoi est trouvé
            try {
                $T = TournoiDAO::find($_REQUEST["tournoi_id"]);
            } catch (\Throwable $th) {
                throw $th;
            }
            if ($T == null) {
                $_REQUEST["message_erreur"] = "Incappable de trouver le tournoi avec le id de {" . $_GET["tournoi_id"] ."}";
                return "ajout-match";
            }
        } else {
            $_REQUEST["message_erreur"] = "Le id du tournoi n'a pas été passé en paramêtre!";
            return "ajout-match";
        }

        //pour les actions
        if (!isset($_REQUEST["actionAdmin"])) { //si l'action est trouvé
            $_REQUEST["message_erreur"] = "L'action de l'administrateur introuvable";
            return "tableau_matchs";
        }
        
        //Saisir le résultat d'un match un match
        if ($_REQUEST["actionAdmin"] == "SaisirResultatMatch") {
            if (isset($_REQUEST["match_id"])) { //si le id du match est trouvé
                try {
                    $M = MatchDAO::find($_REQUEST["match_id"]);
                    if ($M == null) {
                        $_REQUEST["message_erreur"] = "Incappable de trouver le match!";
                        return "tableau_matchs";
                    } else {    //si le match existe bien dans la base de donnée:
                        if (isset($_REQUEST["resultat_equipe1"]) && isset($_REQUEST["resultat_equipe2"]))
                        $M->setPointageEquipe1($_REQUEST["resultat_equipe1"]);
                        $M->setPointageEquipe2($_REQUEST["resultat_equipe2"]);
                        if (!MatchDAO::update($M)) {    //si la deletion du match ne fonctionne pas
                            $_REQUEST["message_erreur"] = "Incappable d'ajouter le résultat du match!";
                            return "tableau_matchs";
                        } else {    //si la suppression du match est réussi!
                            $_REQUEST["message_succes"] = "Résultat du match ajouté!";
                            return "tableau_matchs";

                        }
                    }
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }

        //Supprimer un match
        if ($_REQUEST["actionAdmin"] == "suppMatch") {
            if (isset($_REQUEST["match_id"])) { //si le id du match est trouvé
                try {
                    $M = MatchDAO::find($_REQUEST["match_id"]);
                    if ($M == null) {
                        $_REQUEST["message_erreur"] = "Incappable de trouver le match!";
                        return "tableau_matchs";
                    } else {    //si le match existe bien dans la base de donnée:
                        if (!MatchDAO::delete($M)) {    //si la deletion du match ne fonctionne pas
                            $_REQUEST["message_erreur"] = "Incappable de Supprimer le match!";
                            return "tableau_matchs";
                        } else {    //si la suppression du match est réussi!
                            $_REQUEST["message_succes"] = "Match supprimé!";
                            return "tableau_matchs";

                        }
                    }
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }

        //Éditer le classement d'un équipe dans un tournoi TODO:
        if ($_REQUEST["actionAdmin"] == "EditerClassementEquipe") {
            if (isset($_REQUEST["equipe_id"])) { //si le id du match est trouvé
                try {
                    $E = EquipeDAO::find($_REQUEST["equipe_id"]);
                    if ($E == null) {
                        $_REQUEST["message_erreur"] = "Équipe Introuvable!";
                        return "tableau_matchs";
                    } else {    //si le match existe bien dans la base de donnée:
                        if (isset($_REQUEST["classement_entree"])) {
                            $cl = $_REQUEST["classement_entree"];
                            if ($cl == null || $cl == "" || $cl <= 0) {
                                $_REQUEST["message_erreur"] = "le classement doit être un nombre positif de 1 à 3 chiffres!";
                                return "tableau_matchs";
                            }
                        } else {
                            $_REQUEST["message_erreur"] = "Aucun classement entrée!";
                            return "tableau_matchs";
                        }
                        if (!EquipeDAO::set_classement_equipe($E->getId(), $T->getId(), $cl)) {    //si la deletion du match ne fonctionne pas
                            $_REQUEST["message_erreur"] = "Incappable de modifier le classement!";
                            return "tableau_matchs";
                        } else {    //si la suppression du match est réussi!
                            $_REQUEST["message_succes"] = "Classement modifié!";
                            return "tableau_matchs";
                        }
                    }
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }

        //Supprimer une équipe d'un Tournoi
        if ($_REQUEST["actionAdmin"] == "suppEquipeTournoi") {
            if (isset($_REQUEST["equipe_id"])) { //si le id du match est trouvé
                try {
                    $E = EquipeDAO::find($_REQUEST["equipe_id"]);
                    if ($E == null) {
                        $_REQUEST["message_erreur"] = "Équipe Introuvable!";
                        return "tableau_matchs";
                    } else {    //si le match existe bien dans la base de donnée:
                        if (!EquipeDAO::delete($E)) {    //si la deletion du match ne fonctionne pas
                            $_REQUEST["message_erreur"] = "Incappable de supprimer l'équipe!";
                            return "tableau_matchs";
                        } else {    //si la suppression du match est réussi!
                            $_REQUEST["message_succes"] = "Équipe supprimé du tournoi!";
                            return "tableau_matchs";

                        }
                    }
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }




        






        return "calendrier_tournois";
    }
}
?>