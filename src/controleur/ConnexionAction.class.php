<?php
require_once('./controleur/Action.interface.php');
//importation des classes et DAOs utilisés
require_once('./modele/classes/Compte.class.php');
require_once('./modele/dao/CompteDAO.class.php');

class ConnexionAction implements Action
{
    public function execute()
    {
        if (!$this->valide()) {
            return "connexion";
        }
        
        $cdao = new CompteDAO();
        $compte = $cdao->findByCourriel($_REQUEST["courriel"]);

        if ($compte == null) {
            $_REQUEST["field_messages"]["courriel"] = "Utilisateur inexistant";
            return "connexion";
        } elseif ($compte->verifierMdp($_REQUEST["motPasse"]) == false) {
            $_REQUEST["field_messages"]["motPasse"] = "Mot de passe invalide";
            return "connexion";
        }

        $_SESSION["connecte"]["id"] = $compte->getId();
        $_SESSION["connecte"]["role"] = $compte->getRole();
        $_SESSION["connecte"]["prenom"] = $compte->getPrenom();
        $_SESSION["connecte"]["nom"] = $compte->getNom();

        $_REQUEST["message_succes"] = "Vous êtes connecté!";
        return "calendrier_tournois";
        
    }

    public function valide()
    {
        if ($_REQUEST["courriel"] == "" || !isset($_REQUEST["courriel"])) {
            $_REQUEST["message_erreur"] = "Vous devez entrer votre adresse courriel.";
            return false;
        }

        if ($_REQUEST["motPasse"] == "" || !isset($_REQUEST["motPasse"])) {
            $_REQUEST["message_erreur"] = "Vous devez entrer un mot de passe.";
            return false;
        }
        
        // Si valide après verifications
        return true;
    }
}
