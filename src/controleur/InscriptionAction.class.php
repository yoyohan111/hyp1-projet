<?php

require_once('./controleur/Action.interface.php');
require_once('./controleur/RequirePRGAction.interface.php');
require_once('./modele/dao/CompteDAO.class.php');
require_once('./modele/classes/Compte.class.php');

class InscriptionAction implements Action
{
    public function execute()
    {
        //if (ISSET($_SESSION["_connecte"]))
        //    return "default";

        if (!isset($_REQUEST["prenom"]) ||
                !isset($_REQUEST["courriel"]) ||
                !isset($_REQUEST["motPasse"]) ||
                !isset($_REQUEST["confMotPasse"])) {
            $_REQUEST["message_erreur"] = "Des paramètres sont manquants dans le formulaire!";
            return "inscription";
        }
        
        if ($_REQUEST["prenom"] == '' ||
                $_REQUEST["courriel"] == '' ||
                $_REQUEST["motPasse"] == '' ||
                $_REQUEST["confMotPasse"] == '') {
            $_REQUEST["message_erreur"] = "Veuillez remplir tous les champs.";
            return "inscription";
        }

        if (strlen($_REQUEST["motPasse"]) < 8) {
            $_REQUEST["message_erreur"] = "Votre mot de passe doit comporter au moins 8 caractères.";
            return "inscription";
        }
        
        if ($_REQUEST["motPasse"] != $_REQUEST["confMotPasse"]) {
            $_REQUEST["message_erreur"] = "Le mot de passe et la confirmation du mot de passe doivent être identiques.";
            return "inscription";
        }
        
        $dao = new CompteDAO();
        $c = new Compte();
        
        
        $c->setRole(1);
        $c->setCourriel($_REQUEST["courriel"]);
        $c->setMdp($_REQUEST["motPasse"]);
        $c->setPrenom($_REQUEST["prenom"]);
        $c->setNom($_REQUEST["nom"]);
        $c->hasherMdp();

        if (!$dao->create($c)) {
            $_REQUEST["message_erreur"] = "Un compte est d&eacute;j&agrave; enregistr&eacute; &agrave; cette adresse.";
            return "inscription";
        }

        $_REQUEST["message_succes"] = "Compte créé avec succès!";
        return "connexion";
    }
}
