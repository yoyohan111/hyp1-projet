<?php
/**
 * Interface d'action
 *
 */
interface Action {
	public function execute();
}
?>
