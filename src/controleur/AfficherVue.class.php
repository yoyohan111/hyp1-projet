<?php
require_once('./controleur/Action.interface.php');
class AfficherVue implements Action {
    public function execute() {
        if (isset($_REQUEST["vue"])) {
            if ($_REQUEST["vue"] == "connexion" || $_REQUEST["vue"] == "inscription") {
                $_POST['content-class'] = 'bg-secondary';   //pour la couleur de background
            }
            return $_REQUEST["vue"];
        } else {
            return "info";
        }
        
    }
}
