<?php

/**
 * Reçoit l'action donnée par l'utilisateur et retourne la vue correspondante.
 *
 */
//inclure le default action s'il n'y a pas d'action
require_once('./controleur/DefaultAction.class.php');
//inclure tout les vues
require_once('./controleur/AfficherVue.class.php');
//Actions de connexions:
require_once('./controleur/ConnexionAction.class.php');
require_once('./controleur/InscriptionAction.class.php');
require_once('./controleur/DeconnexionAction.class.php');
//Actions de l'Administrateur:
require_once('./controleur/InscrireEquipeAction.class.php');
require_once('./controleur/CreerTournoiAction.class.php');
require_once('./controleur/AjoutMatchAction.class.php');
require_once('./controleur/ActionsAdmin.class.php');


class ActionBuilder
{
    public static function getAction($nom)
    {
        switch ($nom) {
            //login - logout - inscription
            case "vue":
                return new AfficherVue();
                break;
            case "connexion":
                return new ConnexionAction();
                break;
            case "inscription":
                return new InscriptionAction();
                break;
            case "deconnexion":
                return new DeconnexionAction();
                break;

            //actions
            case "inscrireEquipe":
                return new InscrireEquipeAction();
                break;
            case "creerTournoi":
                return new CreerTournoiAction();
                break;
            case "ajoutMatch":
                return new AjoutMatchAction();
                break;
            case "actionsAdmin":
                return new ActionsAdmin();
                break;


            
            default:
                $_SESSION['menu'] = "default";
                return new DefaultAction();
        }
    }
}
